package uniandes.isis2304.epsandes.negocio;

public interface VOMedicoSS {

	/**
	 * @return la especialidad del medico
	 */
	public long idMedico();
	
	/**
	 * @return el id del medico
	 */
	public long idServicio();
	
}
