package uniandes.isis2304.epsandes.negocio;

public interface VOIPSTipoSS {
	
	
	public long getId();
	
	public String getTipoSS();
	
	public int getCapacidad();

}